#!/bin/bash
clear

function maquinas()
{
    docker run -e "WEBAPP_ROOT=public_html" -v $PWD:/app -e WEBAPP_USER_ID=$(id -u) -p 80:80 -p 443:443 enyalius/dev:latest
}

function maquinas(){
    docker-compose up
}

if [ -f ".ready" ]
then
    echo "Pronto para iniciar o ambiente..."    
    maquinas
else
    echo "Parece que é nossa primeira execução vamos realizar algumas tarefas."
    echo "--------"
    echo "Se acontecer algum erro por aqui verifique se você tem o git devidamente instalado em seu computador."
    git submodule init
    git submodule update
    echo "Agora que você tem tudo pronto vou passar a bola para o Enyalius. "
    if which eny >/dev/null; then
        echo 'O enyalius está instalado na versão'
        eny --version
        echo 'Essa versão deve ser superior ou igual a' 
        grep '^# Versão ' "$0" | tail -1 | cut -d : -f 1 | tr -d \#
    else
        ./core/enyalius localinstall
    fi
    eny gitconfig --noforce
    eny -i
    echo "--------"
    echo "Pronto para iniciar o ambiente."
    maquinas
    touch .ready
fi
