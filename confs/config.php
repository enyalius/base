<?php
/**
 * Arquivo com as configurações do sistema em modo de desenvolvimento
 *  nesse arquivo fica as constantes responsáveis pelo funcionamento correto do sistema. 
 * 
 * Configurações de usuário e senha de Banco de dados por padrão não ficam nesse arquivo.
 * 
 * Em produção recomenda se reescrever removendo as condicionais.
 *
 * @author NOME <EMAIL>
 */

 if(is_file(__DIR__ . '/env.php')){
    require __DIR__ . '/env.php';
}
$base =  str_replace('confs/config.php','',__FILE__);

//Constante que define o caminho onde fica o diretorio dos módulos do sistema
define('ROOT', $base. 'app/');

//Constante que define o caminho onde fica o framework servidor do Enyalius
$eny = getenv('ENY') ? $base . 'core/': $base . 'vendor/enyalius/core/';
define('CORE', $eny);

//Constante que define o caminho onde fica o diretorio publico da aplicação
define('PUBLIC_DIR', $base . 'www/');

//Constante que define onde ficará os templates do sistema
define('TEMPLATES', ROOT . 'view/templates/');

//Constante para o Framework Smarty utilizar como cache, e outros frameworks usarem
//para armazenar arquivos de cache.
define('CACHE', $base . 'z_data/');

//Constante para o diretorio de logs
define('LOGS', CACHE . 'logs');

//Constante para a definição se o sistema esta em produção ou teste
$debug = !getenv('PRODUCAO');
define('DEBUG', $debug);
define('DEVEL', true); //Diferente do debug não pode ser colocado como true em ambiente de produção

//define a chave de critpografia
define('LOGIN_CHAVE', 'SENHA'); #TODO 

//Configurações de hora
date_default_timezone_set('America/Sao_Paulo');

//Configuração de formato
setlocale(LC_ALL, 'pt_BR');

//Demais configurações
require __DIR__ . '/conf_mail.php';
require __DIR__ . '/conf_bd.php';
