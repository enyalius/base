<?php

/**
 * Arquivo que apresenta as configurações de banco de dados.
 *
 * @author NOME <EMAIL>
 * @version 1.0
 * @package
 */


//Constante que define o servidor
define('DB_SERVER', SERVERBANCO);

//Constante que define a porta do banco de dados
define('DB_PORT', '5432');

//Constante que define o usuário do banco de dados
define('DB_NAME', BANCO);

//Constante que define o usuário do banco de dados
define('DB_USER', USUARIOBANCO);

//Constante que define o usuário do banco de dados
define('DB_PASSWORD', SENHABANCO);

define('DB_TYPE', 'pgsql');



