<?php

/**
 * Arquivo que apresenta as configurações para o envio de e-mails .
 *
 * @author NOME <EMAIL>
 * @version 1.0
 * @package confs
 */
define('MAIL_SERVER', 'smtp.gmail.com');
define('MAIL_PORT', '587');
define('MAIL_USER', 'EMAIL');
define('MAIL_PASS', 'SENHA');